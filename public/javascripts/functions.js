/**
 * Created by gadossmann on 26.08.15.
 */

var projectPlato = {

    'Apple': {
        //Browsers
        //OS
        'OS_mac': 1,

        'data': {
            'image': '/images/magicmouse.jpg',
            'product': "MagicMouse"
        }
    },

    'Windows': {
        //Browsers
        //OS
        'OS_windows': 1,

        'data': {
            'image': '/images/macbook.jpg',
            'product': "MacBook Pro"
        }
    },

    'Linux': {
        //OS
        'OS_linux': 1,
        'data': {
            'image': '/images/macbook.jpg',
            'product': "MacBook Pro"
        }
    },
    'currentUser': {}

}

var productsRate = [
    "Windows"
];

function getBrowserInfo() {
    var browser = "";

    if (navigator.userAgent.indexOf("Chrome") != -1) {
        browser = "browser_chrome";
    }
    else if (navigator.userAgent.indexOf("Opera") != -1) {
        browser = "browser_opera";
    }
    else if (navigator.userAgent.indexOf("Safari") != -1) {
        browser = "browser_safari";
    }
    else if (navigator.userAgent.indexOf("Firefox") != -1) {
        browser = "browser_firefox";
    }
    else if ((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) //IF IE > 10
    {
        browser = "browser_ie";
    }

    return browser
}

function getOSInfo() {
    var OSName = "";

    if (navigator.appVersion.indexOf("Win") != -1) OSName = "OS_windows";
    if (navigator.appVersion.indexOf("Mac") != -1) OSName = "OS_mac";
    if (navigator.appVersion.indexOf("X11") != -1) OSName = "OS_linux";

    return OSName;
}
function getProduct() {
    var attributes = [];

    attributes.push(getBrowserInfo());

    attributes.push(getOSInfo());

    for (var i = 0; i < attributes.length; i++) {
        if (attributes[i] != "") projectPlato.currentUser[attributes[i]] = 1;
    }

    var top = topMatches(projectPlato, "currentUser", undefined, sim_distance);
    var product = chooseBest(top);
    console.log(top);
    $("h3").text(projectPlato[product.name].data.product);
    $("img").attr("src", projectPlato[product.name].data.image);

}

function chooseBest(result) {
    var arr = [];
    for (var i = 0; i < result.length; i++) {
        arr.push(result[i]);
        if (result[i + 1]) {
            if (result[i].value != result[i + 1].value) {
                break;
            }
        }
    }

    arr.sort(function (a, b) {
        var rate1 = productsRate.indexOf(a.name);
        var rate2 = productsRate.indexOf(b.name);
        return rate2 - rate1;
    });

    return arr[0];
}
//function getScaleBrowser(browser, scale) {
//    scale = scale || 5;
//    var browsers = {
//        "chrome": 2.0,
//        "safari": 3.0,
//        "mozilla": 4.0
//    }
//
//    if (browsers.hasOwnProperty(browser)) {
//        return browsers[browser];
//    }
//    return 0;
//}

function sim_distance(array, v1, v2) {
    var result = 0,
        isAnyCommon = false;
    //идем по всем свойствам обьекта v1
    for (var key in array[v1]) {
        //ищем свойства v1 в v2
        for (var object in array[v2]) {
            if (key === object && object != 'data') {
                isAnyCommon = true;
                result += Math.pow(array[v2][key] - array[v1][key], 2);

                console.log(result);
            }
        }
    }

    //правильная функция
    //return isAnyCommon ? 1 / (1 + Math.sqrt(result)) : 0;

    //из учебника с ошибкой
    return isAnyCommon ? 1 / (1 + result) : 0;
}

//почему -1 для Toby & Phillips? Они же похожи...
function sim_pearson(array, v1, v2) {
    var result = 0,
        lengthOfCommon = 0,
        sum1 = 0,
        sum2 = 0,
        sum1Sq = 0,
        sum2Sq = 0,
        pSum = 0;


    //идем по всем свойствам обьекта v1
    for (var key in array[v1]) {
        //ищем свойства v1 в v2
        for (var object in array[v2]) {
            if (key === object) {
                lengthOfCommon++;
                sum1 += array[v1][key];
                sum2 += array[v2][key];
                sum1Sq += Math.pow(array[v1][key], 2);
                sum2Sq += Math.pow(array[v2][key], 2);
                pSum += array[v1][key] * array[v2][key];
            }
        }
    }

    if (lengthOfCommon == 0) {
        return 0;
    }

    var num = pSum - (sum1 * sum2 / lengthOfCommon),
        den = Math.sqrt((sum1Sq - Math.pow(sum1, 2) / lengthOfCommon) * (sum2Sq - Math.pow(sum2, 2) / lengthOfCommon));

    if (den == 0) {
        return 0;
    }


    result = num / den;
    return result;
}

function topMatches(array, v1, n, simularity) {
    var result = [];
    n = n || 5;
    simularity = simularity || sim_pearson;

    if (!array.hasOwnProperty(v1)) {
        return "Нет такого обьекта в data";
    }
    for (var v2 in array) {
        if (v2 != v1) {
            result.push({"value": simularity(array, v1, v2), "name": v2});
        }
    }

    result.sort(function (a, b) {

        return b.value - a.value;
    });

    return result.slice(0, n);

}

//в чем логика убирать всех у кого sim <0?
function getRecommendations(array, v1, similarity) {

    var result = [];

    if (!array.hasOwnProperty(v1)) {
        return "Нет такого обьекта в data";
    }

    similarity = similarity || sim_pearson;
    var allOtherPropertiesToRecoomend = {};

    for (var v2 in array) {
        //ищем свойства v1 в v2
        if (v2 == v1) {
            continue;
        }
        var sim = similarity(array, v1, v2);

        if (sim < 0) {
            continue;
        }

        for (var v2Elems in array[v2]) {
            if (array[v1].hasOwnProperty(v2Elems)) {
                continue;
            }

            if (!allOtherPropertiesToRecoomend.hasOwnProperty(v2Elems)) {
                allOtherPropertiesToRecoomend[v2Elems] = {};
                allOtherPropertiesToRecoomend[v2Elems].totalScore = 0;
                allOtherPropertiesToRecoomend[v2Elems].evaluators = 0;
            }

            allOtherPropertiesToRecoomend[v2Elems].totalScore += sim * array[v2][v2Elems];
            allOtherPropertiesToRecoomend[v2Elems].evaluators += sim;
        }

    }

    for (var element in allOtherPropertiesToRecoomend) {
        result.push({
            "value": allOtherPropertiesToRecoomend[element].totalScore / allOtherPropertiesToRecoomend[element].evaluators,
            "name": element
        })
    }

    result.sort(function (a, b) {
        return b.value - a.value;
    });

    return result;

}

function getRecommendedItems(array, patterns, v1) {
    var v1Attributes = array[v1],
        scores = {},
        totalSim = {},
        result = [];

    for (var atrribute in v1Attributes) {
        for (var i = 0; i < patterns[atrribute].length; i++) {
            var similarFromPatterns = patterns[atrribute][i].name;

            if (v1Attributes.hasOwnProperty(similarFromPatterns)) {
                continue;
            }

            if (!scores.hasOwnProperty(similarFromPatterns)) {
                scores[similarFromPatterns] = 0;
                totalSim[similarFromPatterns] = 0;
            }

            scores[similarFromPatterns] += patterns[atrribute][i].value * v1Attributes[atrribute];
            totalSim[similarFromPatterns] += patterns[atrribute][i].value;
        }
    }

    for (var element in scores) {
        result.push({"value": scores[element] / totalSim[element], "name": element});
    }

    result.sort(function (a, b) {
        return b.value - a.value;
    });

    return result;

}

function createPatterns(array, n) {

    n = n || 10;

    var result = {};
    array = transponate(array);
    var c = 0;
    var length = Object.keys(array).length;
    for (var key in array) {
        c += 1;
        (c % 10) == 0 ? console.log(c + " / " + length) : 0;

        var reccomendations = topMatches(array, key, n, sim_distance);
        result[key] = reccomendations;
    }

    console.log("Patterns has been created");
    return result;
}

function transponate(array) {
    var result = {};

    for (var object in array) {
        for (var property in array[object]) {
            if (!result.hasOwnProperty(property)) {
                result[property] = {};
            }
            result[property][object] = array[object][property];
        }
    }

    console.log("Objects was transponated");
    return result;
}

function getMovieLens() {
    var result = JSON.parse($.ajax({
        type: "GET",
        url: "api/getMovieLens",
        async: false //чит
    }).responseText);

    console.log("MovieLens was loaded");
    return result;
}

//function initializeUserDict(tag, count) {
//    var users = {};
//    var links = JSON.parse($.ajax({
//        type: "POST",
//        url: "api/getLinks",
//        data: {"tag": tag},
//        async: false //чит
//    }).responseText);
//
//
//    for (var i = 0; i < links.length; i++) {
//        links[i].hash = md5(links[i].u);
//    }
//    console.log(links);
//    //data.slice(0,count);
//    //for (var i = 0; i < links.length; i++) {
//    //    var users = $.parseJSON($.ajax({
//    //        type: "POST",
//    //        url: "api/getUsers",
//    //        data: {"url": links[i]},
//    //        async: false //чит
//    //    }).responseText);
//    //}
//
//
//}


