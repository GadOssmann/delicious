var express = require('express'),
    router = express.Router(),
    request = require('request'),
    fs = require("fs");

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {title: 'Откройте браузерную консоль для ввода функций / или же консоль Node.'});
});


router.get('/api/getMovieLens', function (req, res, next) {
    var result = getMovieLens();
    res.send(result);
});

function getMovieLens() {
    var result = {}
    var movies = {};

    var moviesFile = fs.readFileSync('model/u.item').toString().split("\n");
    for (var i = 0; i < moviesFile.length; i++) {
        var lineSplit = moviesFile[i].split("|");
        if (lineSplit[0] != '') {
            movies[lineSplit[0]] = lineSplit[1];
        }
    }

    var dataFile = fs.readFileSync('model/u.data').toString().split("\n");
    for (var i = 0; i < dataFile.length; i++) {
        var lineSplit = dataFile[i].split("\t");
        if (lineSplit[0] != '') {
            var lineSplit = dataFile[i].split("\t");
            if (!result.hasOwnProperty(lineSplit[0])) {
                result[lineSplit[0]] = {};
            }

            result[lineSplit[0]][movies[lineSplit[1]]] = parseFloat(lineSplit[2]);
        }
    }
    return result;
}


//router.post('/api/getLinks', function (req, res, next) {
//
//    authDelicious();
//    request('http://feeds.delicious.com/v2/json/tag/' + req.body.tag, function (error, response, body) {
//        if (!error && response.statusCode == 200) {
//            res.send(body);
//        }
//    })
//
//});

//authDelicious();
//function authDelicious(){
//
//    //11173082-52bba050ff6a6937923ae6e1cd0d66d2
//
//         request.post('https://avosapi.delicious.com/api/v1/oauth/token?client_id=d883baf606834eb77e4c1c6df5f52ea7&client_secret=744a5ecb174407bb125693030c235ca0&grant_type=code&code=299c53ec1f33ae3c9e022fe30d2294a6', function (error, response, body) {
//            if (!error && response.statusCode == 200) {
//                console.log(body);
//            }else{
//                console.log(error);
//            }
//        })
//}
//
//router.post('/api/getUsers', function (req, res, next) {
//
//
//    request('http://feeds.delicious.com/v2/json/tag/' + req.body.tag, function (error, response, body) {
//        if (!error && response.statusCode == 200) {
//            res.send(body);
//        }
//    })
//
//});


module.exports = router;
